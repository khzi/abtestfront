
import React from "react";
import ReactDOM from "react-dom";
import ReactDataGrid from "react-data-grid";
import Histogram from 'react-chart-histogram';

import axios from 'axios';

import "./styles.css";

const DateFormatter = ({ value }) => {
    return (new Date(value)).toUTCString();
};

const DateEditor = '<input type="date">';

const columns = [
    { key: "UserId", name: "User Id", editable: true },
    { key: "RegistrationDate", name: "Date Registration", editable: true, formatter: DateFormatter, editor: DateEditor },
    { key: "LastActivityDate", name: "Date Last Activity", editable: true, formatter: DateFormatter, editor: DateEditor },
    { key: "action", name: "Action" }
];

class ActivityTable extends React.Component {
    state = { records: [] };

    constructor(props) {
        super(props);
        this.addEmptyRow = this
            .addEmptyRow
            .bind(this);

        this.saveTable = this
            .saveTable
            .bind(this);

        this.calculate = this
            .calculate
            .bind(this);
    }

    getCellActions = (column, row) => {
        const cellActions = [
            {
                icon: <span className="glyphicon glyphicon-remove" />,
                callback: () => {
                    const rows = [...this.state.records];
                    rows.splice(rows.findIndex((x) => x.Id === row.Id), 1); //
                    this.setState({ records: rows });
                }
            }
        ];
        return column.key === "action" ? cellActions : null;
    };

    onGridRowsUpdated = ({ fromRow, toRow, updated }) => {
        this.setState(state => {
            const records = state.records.slice();
            updated.UserId = 1 * updated.UserId;
            for (let i = fromRow; i <= toRow; i++) {
                records[i] = { ...records[i], ...updated };
            }
            return { records: records };
        });
    };

    componentDidMount() {
        axios.get(`http://localhost:59930/useractivity/list`)
            .then(res => {
                const records = res.data;
                this.setState({ records: records });
            })
    }

    getNextStateId() {
        var ans = -1;

        for (var i = 0; i < this.state.records.length; i++) {
            ans = Math.max(ans, this.state.records[i].Id);
        }

        return ans + 1;
    }

    addEmptyRow() {
        this.setState(state => {
            const records = state.records.slice();
            records.push({
                Id: this.getNextStateId(),
                RegistrationDate: new Date(),
                LastActivityDate: new Date()
            });
            return { records };
        });
    }

    saveTable() {
        const headers = {
            'Content-Type': 'application/json',
        };

        axios.post(`http://localhost:59930/useractivity/store`, JSON.stringify(this.state.records)
            , { headers: { "Content-Type": 'application/json'} }).then(response => {
            console.log(response);
        });
    }

    calculate() {
    }

    render() {
        return (
             <div>
                <button onClick={this.addEmptyRow}>Insert empty row</button>
                <ReactDataGrid
                    columns={columns}
                    rowGetter={i => this.state.records[i]}
                    rowsCount={this.state.records.length}
                    onGridRowsUpdated={this.onGridRowsUpdated}
                    enableCellSelect={true}
                    getCellActions={this.getCellActions}
                />
                <button onClick={this.saveTable}>Save</button>
                <button onClick={this.calculate}>Calculate</button>
            </div>
        );
    }
}

class Statistics extends React.Component {
    state = { labels: 0, data: 0 };
    stateRetention = { returnedUsers: 0, installedUsers: 0 };

    componentDidMount() {
        axios.get(`http://localhost:59930/useractivity/lifetimedistribution`)
            .then(res => {
                const records = res.data;
                var labels = records.map(function (v) {
                    return v.Lifetime;
                });

                var data = records.map(function (v) {
                    return v.UsersCount;
                });

                this.setState({ labels, data });
            })

        axios.get(`http://localhost:59930/useractivity/rollingretention`)
            .then(res => {
                const records = res.data;
                this.setStateRetention({ records });
            })

    }

    render() {
        const options = { fillColor: '#FFFFFF', strokeColor: '#0000FF' };
        return (
            <div>
                <h1>Rolling retention 7</h1>
                {this.state.returnedUsers / this.state.installedUsers * 100} %

                <Histogram
                    xLabels={this.state.labels}
                    yValues={this.state.data}
                    width='400'
                    height='200'
                    options={options}
                />
            </div>
        )
    }
}

class TestTask extends React.Component {
    render() {
        return (
            <div>
                <ActivityTable />
                <Statistics />
            </div>
        );
    }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<TestTask />, rootElement);
